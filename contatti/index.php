<?php include "../header.html"; ?>
        <title>Contatti - Fedé - Estetica & Dedizione | Vittorio Veneto</title><!--titolo-->
    </head>
    <body class="contatti">
    <?php // include "menu.html"; ?>
        <?php include "../menu.php"; ?>
        <div id="container">
        <!----------------------------------------------------------------------->
        <!-------------------------- Desktop block ------------------------------>
        <!----------------------------------------------------------------------->
        <div id="container-desktop">
            <div data-anchor="home 1" class="snap first">
                <div class="container-fluid">
                    <div class="logo">
                        <a href="/" title="home"><img src="/fede2/img/logo-scuro.svg" alt="logo"></a>
                    </div>        
                    <div class="row h-100 align-items-md-end justify-content-between">
                        <div class="col-md-6">
                            <h2>
                                <div><span>INFO</span></div>
                                <div><span>CONTATTI</span></div>
                                <div><span>BOOKING</span></div>
                            </h2>
                        </div>
                        <div class="col-md-6">
                            <form enctype="multipart/form-data" action="index.php" method="post">
                                <input class="form-control" type="text" placeholder="Nome e Cognome">
                                <input class="form-control" type="email" placeholder="Email">
                                <input class="form-control" type="tel" placeholder="Telefono">
                                <textarea class="form-control" placeholder="Richiesta"></textarea>
                                <input type="submit" id="submit" value="Invia">
                            </form>
                            <div class="icon">
                                <img src="../img/gps.png" alt="icon">
                                <a href="https://goo.gl/maps/LZ41ajHFM8asMP9j7" target="_blank" title="posizione">Via Scrizzi, 27 - Vittorio Veneto (TV)</a>
                            </div>
                            <div class="icon">
                                <img src="../img/icon.png" alt="icon">
                                <a href="tel:+393386251805" title="cellulare">338 6251805 |</a>  
                                <a href="mailto:info@fedeestetica.com" title="mail">info@fedeestetica.com</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="footer" class="snap">
                <?php include "../footer.html"; ?>
            </div>
        </div>
            <!----------------------------------------------------------------------->
            <!-------------------------- Mobile block ------------------------------>
            <!----------------------------------------------------------------------->
        <div id="container-mobile">
            <div data-anchor="servizi mobile 1" class="section first">
                <div class="container-fluid">
                    <div class="logo">
                        <a href="/" title="home"><img src="/fede2/img/logo-scuro.svg" alt="logo"></a>
                    </div>        
                    <div class="row h-100 align-items-center">
                        <div class="col-12">
                            <h2>
                                <div><span>INFO</span></div>
                                <div><span>CONTATTI</span></div>
                                <div><span>BOOKING</span></div>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="servizi mobile 2" class="section first">
                <div class="container-fluid">
                    <div class="row h-100 align-items-center">
                        <div class="col-12">
                            <form enctype="multipart/form-data" action="index.php" method="post">
                                <input class="form-control" type="text" placeholder="Nome e Cognome">
                                <input class="form-control" type="email" placeholder="Email">
                                <input class="form-control" type="tel" placeholder="Telefono">
                                <textarea class="form-control" placeholder="Richiesta"></textarea>
                                <input type="submit" id="submit" value="Invia">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="list servizi mobile" class="section first margin-mobile">
                <div class="container-fluid">
                    <div class="row h-100 align-items-center">
                        <div class="col-12">
                            <div class="icon">
                                <img src="../img/gps.png" alt="icon">
                                <a href="" title="posizione">Via Scrizzi, 27 - Vittorio Veneto (TV)</a>
                            </div>
                            <div class="icon">
                                <img src="../img/icon.png" alt="icon">
                                <a href="tel:+393386251805" title="cellulare">338 6251805</a>  
                            </div>
                            <div class="icon">
                                <img src="../img/mail.png" alt="icon">
                                <a href="mailto:info@fedeestetica.com" title="mail">info@fedeestetica.com</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="footer mobile" class="section footer-mobile">
                <?php include "../footer.html"; ?>
            </div>
            <!----------------------------------------------------------------------->
            <!----------------------------------------------------------------------->
            <!----------------------------------------------------------------------->
        </div>
        <div class="back"><a href="#home-1"><img src="../img/freccia-scura.png" alt="back"></a></div>
    </div>
        <!-- Script -->
        <script src="/fede2/js/jquery-3.4.1.min.js"></script>
        <script src="/fede2/js/cookiechoices.js"></script>
        <script src="/fede2/js/bootstrap.min.js"></script>
        <script src="/fede2/js/pageable.js"></script>
        <script src="/fede2/js/in-view.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.9.1/gsap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.9.1/ScrollTrigger.min.js"></script>
        <script src="/fede2/js/script.js"></script>            
    </body>
</html>
