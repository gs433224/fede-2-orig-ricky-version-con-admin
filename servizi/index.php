<?php include "../header.html"; ?>
        <title>Servizi - Fedé - Estetica & Dedizione | Vittorio Veneto</title><!--titolo-->
    </head>
    <body class="servizi">
    <?php // include "menu.html"; ?>
        <?php include "../menu.php"; ?>
        <div id="container">
        <!----------------------------------------------------------------------->
        <!-------------------------- Desktop block ------------------------------>
        <!----------------------------------------------------------------------->
        <div id="container-desktop">
            <div data-anchor="home 1" class="snap first">
                <div class="container-fluid">
                    <div class="logo">
                        <a href="/" title="home"><img src="/fede2/img/logo-scuro.svg" alt="logo"></a>
                    </div>        
                    <div class="row h-100 align-items-md-end justify-content-between">
                    <div class="col-md-6">
                            <h2>
                                <div><span class="animReveal">ESTETICA PER</span></div>
                                <div><span class="animReveal">LA BELLEZZA</span></div>
                                <div><span class="animReveal">E IL BENESSERE</span></div>
                            </h2>
                            <h3 style="text-transform:uppercase;font-family: 'Oswald', sans-serif;   font-weight: 500; font-style: italic;">TECNOLOGIE </h3>
                                <p>Macchinari di ultima generazione usati in combinazione con prodotti di altissima qualità sono i punti fermi del nostro lavoro.</p>
                        </div>
                        <div class="col-md-6">
                            <div class="overlay">
                                <div class="imgReveal animWidth"></div>
                                <img src="/fede2/img/servizi.jpg" alt="fede">
                            </div>
                            <h3 style="text-transform:uppercase;font-family: 'Oswald', sans-serif;   font-weight: 500; font-style: italic;">Consulenza estetica personalizzata gratuita. </h3>
<p>Crediamo che l’ascolto e l’empatia siano la prima chiave per impostare un percorso efficace al raggiungimento degli obiettivi.  </p>

                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="servizi" class="snap second">
                <div class="container-fluid">
                    <div class="row h-100 align-items-center justify-content-center">
                    <div class="col-lg-9 col-md-10 px-5">
                        

                        <div class="overlay">
                                <div class="imgReveal animWidth"></div>
                                <img src="/fede4-servizi-verticale/img/estetica-trattamenti.jpg" alt="blog">
                            </div>

                            <!-- <img src="/fede4-servizi-verticale/img/estetica-base.jpg" alt="blog"> -->
                            <table>
                                <tbody><tr>
                                    <th><strong>Estetica avanzata</strong></th>                                    
                                </tr>
                                <tr>
                                    <th>Trattamenti viso anti age</th>
                                </tr>
                                <tr>
                                    <th>Rivitalizzanti</th>
                                </tr>
                                <tr>
                                    <th>Esfolianti</th>
                                </tr>
                                <tr>
                                    <th>Radiofrequenza</th>
                                </tr>
                                <tr>
                                    <th>Trattamenti corpo anticellulite</th>
                                    <td>Riducenti</td>
                                    <td>Leviganti</td>
                                    <td>Biokalko</td>
                                    <td>Bioslimming</td>                                    
                                </tr>
                                
                            </tbody></table>

                            








                            
                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="servizi 2" class="snap second">
                <div class="container-fluid">
                    <div class="row h-100 align-items-center justify-content-center">
                    <div class="col-lg-9 col-md-10 px-5">
                        <img src="/fede4-servizi-verticale/img/estetica-base.jpg" alt="blog"> 
                            <!-- <img src="/fede4-servizi-verticale/img/estetica-avanzata.jpg" alt="blog">-->
                            
                            <table>
                                <tbody><tr>
                                    <th><strong>Estetica base</strong></th>                                    
                                </tr>        
                                <tr>
                                    <th>Manicure</th>
                                </tr>                        
                                <tr>                                    
                                    <th>Pedicure</th>
                                </tr>
                                <tr>
                                    <th>Depilazione</th>
                                </tr>                                
                            </tbody></table>
                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="servizi 3" class="snap second">
                <div class="container-fluid">
                    <div class="row h-100 align-items-center justify-content-center">
                    <div class="col-lg-9 col-md-10 px-5">
                            <img src="/fede4-servizi-verticale/img/estetica-avanzata.jpg" alt="blog">
                            <table>
                                <tbody><tr>
                                    <th><strong>EGO</strong></th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <th>fotobiostimolazione</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <th>bioelettrosimolazione</th>
                                    <th></th>
                                </tr>
                                
                            </tbody></table>
                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="footer" class="snap">
                <?php include "../footer.html"; ?>
            </div>
        </div>
            <!----------------------------------------------------------------------->
            <!-------------------------- Mobile block ------------------------------>
            <!----------------------------------------------------------------------->
        <div id="container-mobile">
            <div data-anchor="servizi mobile 1" class="section first">
                <div class="container-fluid">
                    <div class="logo">
                        <a href="/" title="home"><img src="/fede2/img/logo-scuro.svg" alt="logo"></a>
                    </div>        
                    <div class="row h-100 align-items-center">
                        <div class="col-12">
                            <h2>
                                <div><span>ESTETICA PER</span></div>
                                <div><span>LA BELLEZZA</span></div>
                                <div><span>E IL BENESSERE</span></div>
                            </h2>
                            <p>
                            <strong>Tecnologie</strong>: Macchinari di ultima generazione usati in combinazione con prodotti di altissima qualità
sono i punti fermi del nostro lavoro.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="servizi mobile 2" class="section first">
                <div class="container-fluid">
                    <div class="row h-100 align-items-center">
                        <div class="col-12">
                            <div class="overlay">
                                <div class="imgReveal"></div>
                                <img src="/fede2/img/servizi.jpg" alt="fede">
                            </div>
                            <p>
                            <strong>Consulenza estetica personalizzata gratuita</strong>: Crediamo che l’ascolto e l’empatia siano la prima chiave per impostare un percorso
efficace al raggiungimento degli obiettivi.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="list servizi mobile" class="section second margin-mobile">
                <div class="container-fluid">
                    <div class="row h-100 align-items-center">
                        <div class="col-lg-9 col-md-10 px-sm-4">
                            <img src="/fede2/img/estetica-base.jpg" alt="blog">
                            <table>
                                <tr>
                                    <th><strong>Estetica base</strong></th>
                                    <th>Manicure</th>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th>Ricostruzione unghie</th>
                                </tr>
                                <tr>
                                    <th>Manicure</th>
                                    <th>Pedicure</th>
                                </tr>
                                <tr>
                                    <th>Manicure</th>
                                    <th>Ceretta</th>
                                </tr>
                                <tr>
                                    <th>Pedicure</th>
                                    <th>Manicure</th>
                                </tr>
                                <tr>
                                    <th>Epilazione</th>
                                    <th>Ceretta</th>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="list servizi mobile 2" class="section second">
                <div class="container-fluid">
                    <div class="row h-100 align-items-center">
                        <div class="col-lg-9 col-md-10 px-sm-4">
                            <img src="/fede2/img/estetica-avanzata.jpg" alt="blog">
                            <table>
                                <tr>
                                    <th><strong>Estetica avanzata</strong></th>
                                    <th>Manicure</th>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th>Ricostruzione unghie</th>
                                </tr>
                                <tr>
                                    <th>Manicure</th>
                                    <th>Pedicure</th>
                                </tr>
                                <tr>
                                    <th>Manicure</th>
                                    <th>Ceretta</th>
                                </tr>
                                <tr>
                                    <th>Pedicure</th>
                                    <th>Manicure</th>
                                </tr>
                                <tr>
                                    <th>Epilazione</th>
                                    <th>Ceretta</th>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="list servizi mobile 3" class="section second">
                <div class="container-fluid">
                    <div class="row h-100 align-items-center">
                        <div class="col-lg-9 col-md-10 px-sm-4">
                            <img src="/fede2/img/estetica-trattamenti.jpg" alt="blog">
                            <table>
                                <tr>
                                    <th><strong>Trattamenti</strong></th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th>.</th>
                                </tr>
                                <tr>
                                    <th>Manicure</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <th>Ricostruzione unghie</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <th>Pedicure</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <th>Ceretta</th>
                                    <th></th>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="footer mobile" class="section footer-mobile">
                <?php include "../footer.html"; ?>
            </div>
            <!----------------------------------------------------------------------->
            <!----------------------------------------------------------------------->
            <!----------------------------------------------------------------------->
        </div>
        <div class="back"><a href="#home-1"><img src="../img/freccia-scura.png" alt="back"></a></div>
    </div>
        <!-- Script -->
        <script src="../js/jquery-3.4.1.min.js"></script>
        <script src="../js/cookiechoices.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/pageable.js"></script>
        <script src="../js/in-view.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.9.1/gsap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.9.1/ScrollTrigger.min.js"></script>
        <script src="../js/script.js"></script>            
    </body>
</html>
