<?php include "../includes/config_locale.php" ?>
<?php include "../header.html"; ?>
        <title>Racconti di benessere - Fedé - Estetica & Dedizione | Vittorio Veneto</title><!--titolo-->
    </head>
    <body class="federica">
    <?php // include "menu.html"; ?>
        <?php include "../menu.php"; ?>
        <div id="container">
        <!----------------------------------------------------------------------->
        <!-------------------------- Desktop block ------------------------------>
        <!----------------------------------------------------------------------->
        <div id="container-desktop">
            <div data-anchor="home 1" class="snap first">
                <div class="container-fluid">
                    <div class="logo">
                        <a href="/" title="home"><img src="/fede2/img/logo.svg" alt="logo"></a>
                    </div>        
                    <div class="row h-100 align-items-md-end justify-content-between">
                        <div class="col-md-6">
                            <h2>
                                <div><span>SCOPRI IL</span></div>
                                <div><span>CENTRO E</span></div>
                                <div><span>LA MIA</span></div>
                                <div><span>FILOSOFIA</span></div>
                            </h2>
                        </div>
                        <div class="col-md-6">
                            <div class="overlay">
                                <div class="imgReveal"></div>
                                <img src="/fede2/img/page-federica.jpg" alt="fede">
                            </div>
                            <p>
                                Un nuovo progetto sta prendendo forma, parte dal mio cuore per arrivare fino a voi.
                                Una nuova me, un nuovo percorso e un nuovo concetto di estetica professionale.
                                Un nuovo progetto sta prendendo forma, parte dal mio cuore per arrivare fino a voi.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
                <?php $sql = "SELECT * FROM posts ORDER BY date DESC";
                $result = $conn->query($sql); 
                $count = 0;
                while ($row = $result->fetch_assoc()) {
                    $id = $row['id'];
                    $title = $row['title'];
                    $content = $row['content'];
                    $date = $row['date'];
                    $files = explode(",", $row['files']);

                    if ($count % 3 == 0) {
                        echo '<div data-anchor="blog' . $count . '" class="snap second"><div class="container-fluid"><div class="row h-100 align-items-center">';
                      }
                      ?>
                        <div class="col-4 px-5">
                            <a href="blog.php?id=<?php echo $id; ?>" title="<?php echo html_entity_decode($title); ?>">
                            <img class="card-img-top" src="<?php echo '../upload/'. $files[0]; ?>" alt="news" />
                                <p><?php echo date('d/m/Y', strtotime($date)) ?></p>
                                <h4><?php echo html_entity_decode($title); ?></h4>
                            </a>
                        </div>

                      <?php
                      $count++;
                        if ($count % 3 == 0) {
                        echo '</div></div></div>';
                    }                                                        
                ?>
                <?php }
                // Close the last section if needed
                if ($count % 3 != 0) {
                    echo '</div></div></div>';
                }
                ?>
            <div data-anchor="footer" class="snap">
                <?php include "../footer.html"; ?>
            </div>
        </div>
            <!----------------------------------------------------------------------->
            <!-------------------------- Mobile block ------------------------------>
            <!----------------------------------------------------------------------->
        <div id="container-mobile">
            <div data-anchor="federica mobile 1" class="section first">
                <div class="container-fluid">
                    <div class="logo">
                        <a href="/" title="home"><img src="/fede2/img/logo.svg" alt="logo"></a>
                    </div>        
                    <div class="row h-100 align-items-center">
                        <div class="col-12">
                            <h2>
                                <div><span>SCOPRI IL</span></div>
                                <div><span>CENTRO E</span></div>
                                <div><span>LA MIA</span></div>
                                <div><span>FILOSOFIA</span></div>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
            <div data-anchor="federica mobile 2" class="section first mr-5">
                <div class="container-fluid">
                    <div class="row h-100 align-items-center">
                        <div class="col-12">
                            <div class="overlay">
                                <div class="imgReveal"></div>
                                <img src="/fede2/img/page-federica.jpg" alt="fede">
                            </div>
                            <p>
                                Un nuovo progetto sta prendendo forma, parte dal mio cuore per arrivare fino a voi.
                                Una nuova me, un nuovo percorso e un nuovo concetto di estetica professionale.
                                Un nuovo progetto sta prendendo forma, parte dal mio cuore per arrivare fino a voi.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <?php $sql = "SELECT * FROM posts ORDER BY date DESC";
                $result = $conn->query($sql); 
                $count = 0;
                while ($row = $result->fetch_assoc()) {
                    $id = $row['id'];
                    $title = $row['title'];
                    $content = $row['content'];
                    $date = $row['date'];
                    $files = explode(",", $row['files']);

                    if ($count % 1 == 0) {
                        echo '<div data-anchor="blog' . $count . 'mobile' . $count . '" class="section second"><div class="container-fluid"><div class="row h-100 align-items-center">';
                      }
                      ?>
                        <div class="col-10 px-sm-4">
                            <a href="blog.php?id=<?php echo $id; ?>" title="<?php echo html_entity_decode($title); ?>">
                            <img class="card-img-top" src="<?php echo '../upload/'. $files[0]; ?>" alt="news" />
                                <p><?php echo date('d/m/Y', strtotime($date)) ?></p>
                                <h4><?php echo html_entity_decode($title); ?></h4>
                            </a>
                        </div>

                      <?php
                      $count++;
                        if ($count % 1 == 0) {
                        echo '</div></div></div>';
                    }                                                        
                ?>
                <?php }
                // Close the last section if needed
                if ($count % 1 != 0) {
                    echo '</div></div></div>';
                }
                ?>

            <div data-anchor="footer mobile" class="section footer-mobile">
                <?php include "../footer.html"; ?>
            </div>
            <!----------------------------------------------------------------------->
            <!----------------------------------------------------------------------->
            <!----------------------------------------------------------------------->
        </div>
        <div class="back"><a href="#home-1"><img src="../img/freccia.png" alt="back"></a></div>
    </div>
        <!-- Script -->
        <script src="/fede2_orig/js/jquery-3.4.1.min.js"></script>
        <script src="/fede2_orig/js/cookiechoices.js"></script>
        <script src="/fede2_orig/js/bootstrap.min.js"></script>
        <script src="/fede2_orig/js/pageable.js"></script>
        <script src="/fede2_orig/js/in-view.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.9.1/gsap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.9.1/ScrollTrigger.min.js"></script>
        <script src="/fede2_orig/js/script.js"></script>            
    </body>
</html>
